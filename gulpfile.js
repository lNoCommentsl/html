'use strict';

let gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    path = {
        src: {
            style: 'src/css/main.sass'
        },
        build: {
            style: 'css/'
        },
        watch: {
            style: 'src/css/**/*.sass'
        }
    };

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch(path.watch.style, gulp.series('style'));
    gulp.watch("*.html").on('change', browserSync.reload);

});

gulp.task('style', function() {
    return gulp.src(path.src.style)
    .pipe(sass())
    .pipe(autoprefixer({
        browsers: ['last 5 version']
    }))
    .pipe(gulp.dest(path.build.style))
});

gulp.task('watch', function () {
    gulp.watch(path.watch.style, gulp.series('style'),browserSync.reload);
    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('default', gulp.series('style',gulp.parallel('watch')));
